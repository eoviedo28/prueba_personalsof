Rails.application.routes.draw do
  resources :empresas
  resources :empleados
  get '/empresas/:id/empleados(.:format)', to: 'empresas#get_empleados' 
  get '/logs', to: 'logs#index' 
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
