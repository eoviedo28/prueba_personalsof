ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require 'bundler/setup' # Set up gems listed in the Gemfile.
require 'bootsnap/setup' # Speed up boot time by caching expensive operations.


def sort_query(params)

	if params['sort'].present?
  		if params['sort'].include?('-')
  			sort = params['sort'].gsub!(/\W+/, '')
  			sort = sort+" DESC"
  		else
  			sort = params['sort']
  		end
  	else
  		sort = 'id'
  	end
	sort
end