class EmpleadosController < ApplicationController
  def index
  	params[:page] = params[:page].present? ? params[:page] : 1
  	params[:limit] = params[:limit].present? ? params[:limit] : 100
  	sort = sort_query(params)
  	@empleados = Empleado.all()
  						 .order(sort)
  						 .paginate(page: params[:page], :per_page => params[:limit])

  	respond_to do |format|
  		format.html
  		format.json { render json: @empleados}
  	end
  end

  def show
  	@empleado = Empleado.find(params[:id])
  	respond_to do |format|
  		format.html
  		format.json { render json: @empleado}
  	end
  end

  def new
  	@empleado =  Empleado.new
  end

  def create
  	@empleado =  Empleado.create(empleado_params)
  	respond_to do |format|
  		format.html { redirect_to empleado_path(@empleado) }
  		format.json { render json: @empleado }
  	end
  end

  def edit
  	@empleado = Empleado.find(params[:id])
  	respond_to do |format|
  		format.html
  		format.json { render json: @empleado}
  	end
  end

  def update
  	@empleado = Empleado.find(params[:id])
  	@empleado.update(empleado_params)

  	respond_to do |format|
  		format.html { redirect_to empleado_path(@empleado) }
  		format.json { render json: @empleado}
  	end
  end

  def destroy
  	@empleado = Empleado.find(params[:id])
  	@empleado.destroy
  	respond_to do |format|
  		format.html { redirect_to empleados_path }
  		format.json { redirect_to empleados_path }
  	end
  end

  private

  def empleado_params
  	params.require(:empleado).permit(:nombre, :apellido, :tipo_identificacion, :indentificacion, :telefono, :correo, :empresa_id)
  end
end
