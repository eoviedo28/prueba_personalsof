class ApplicationController < ActionController::Base

after_action :audit_request

def audit_request
	Log.create(ip:  request.remote_ip, http_method: request.method, http_path: request.path, request_data: params)
end

end
