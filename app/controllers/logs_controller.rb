class LogsController < ApplicationController
  def index

  	@logs = Log.all
  	respond_to do |format|
  		format.html
  		format.json { render json: @logs}
  	end
  end
end
