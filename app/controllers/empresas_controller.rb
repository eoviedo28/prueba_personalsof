class EmpresasController < ApplicationController
  def index

  	params[:page] = params[:page].present? ? params[:page] : 1
  	params[:limit] = params[:limit].present? ? params[:limit] : 100
  	sort = sort_query(params)
  	@empresas = Empresa.all()
  					.order(sort)
  					.paginate(page: params[:page], :per_page => params[:limit])

  	respond_to do |format|
  		format.html
  		format.json { render json: @empresas}
  	end
  end

  def show
  	@empresa = Empresa.find(params[:id])
  	respond_to do |format|
  		format.html
  		format.json { render json: @empresa}
  	end
  end

  def new
  	@empresa =  Empresa.new
  end

  def create
  	@empresa =  Empresa.create(empresa_params)
  	respond_to do |format|
  		format.html { redirect_to empresa_path(@empresa) }
  		format.json { render json: @empresa}
  	end
  end

  def edit
  	@empresa = Empresa.find(params[:id])
  	respond_to do |format|
  		format.html
  		format.json { render json: @empresa}
  	end
  end

  def update
  	@empresa = Empresa.find(params[:id])
  	@empresa.update(empresa_params)

  	respond_to do |format|
  		format.html { redirect_to empresa_path(@empresa) }
  		format.json { render json: @empresa}
  	end
  end

  def destroy
  	@empresa = Empresa.find(params[:id])
  	@empresa.destroy
  	respond_to do |format|
  		format.html { redirect_to empresas_path }
  		format.json { redirect_to empresas_path }
  	end
  end

  def get_empleados
  	sort = sort_query(params)
  	@empleados = Empresa.find(params[:id]).empleado.order(sort)
  	respond_to do |format|
  		format.html
  		format.json { render json: @empleados}
  	end
  end

  private

  def empresa_params
  	params.require(:empresa).permit(:nombre, :nit, :direccion, :telefono)

  end
end
