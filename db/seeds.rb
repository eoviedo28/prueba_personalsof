# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
=begin
Empresa.create([{nombre: 'Empresa 1', nit: '0000001', direccion: 'calle 1 # 1 - 00', telefono: '123456789'},
				{nombre: 'Empresa 2', nit: '0000002', direccion: 'calle 1 # 1 - 00', telefono: '123456789'},
				{nombre: 'Empresa 3', nit: '0000003', direccion: 'calle 1 # 1 - 00', telefono: '123456789'}
				])
=end
Empleado.create([
	{nombre: 'Estiven', apellido: 'oviedo', tipo_identificacion: 'CC', indentificacion: '123456789', telefono: '3167873319', correo: 'estiovi@hotmail.com', empresa_id: 1}])