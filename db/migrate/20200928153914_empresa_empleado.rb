class EmpresaEmpleado < ActiveRecord::Migration[6.0]
  def change

  	create_table :empresas do |t|
  		t.string :nombre 
  		t.string :nit
  		t.string :direccion
  		t.string :telefono
	    t.timestamps
    end

  	create_table :empleados do |t|
  		t.string :nombre 
  		t.string :apellido
  		t.string :tipo_identificacion
  		t.string :indentificacion
  		t.string :telefono
  		t.string :correo
  		t.integer :empresa_id
  		t.index [:empresa_id]
	    t.timestamps
    end

  	create_table :log do |t|
	    t.string :ip, limit: 45
	    t.string :http_method, limit: 8, null: false
	    t.string :http_path, null: false
	    t.json :request_data
	    t.timestamps
    end

    add_foreign_key "empleados", "empresas", name: "fk_empleados_empresas"

  end
end
